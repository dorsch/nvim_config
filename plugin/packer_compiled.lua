-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

_G._packer = _G._packer or {}
_G._packer.inside_compile = true

local time
local profile_info
local should_profile = false
if should_profile then
  local hrtime = vim.loop.hrtime
  profile_info = {}
  time = function(chunk, start)
    if start then
      profile_info[chunk] = hrtime()
    else
      profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
    end
  end
else
  time = function(chunk, start) end
end

local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end
  if threshold then
    table.insert(results, '(Only showing plugins that took longer than ' .. threshold .. ' ms ' .. 'to load)')
  end

  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/alex/.cache/nvim/packer_hererocks/2.1.1700008891/share/lua/5.1/?.lua;/home/alex/.cache/nvim/packer_hererocks/2.1.1700008891/share/lua/5.1/?/init.lua;/home/alex/.cache/nvim/packer_hererocks/2.1.1700008891/lib/luarocks/rocks-5.1/?.lua;/home/alex/.cache/nvim/packer_hererocks/2.1.1700008891/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/alex/.cache/nvim/packer_hererocks/2.1.1700008891/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  ["nui.nvim"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/nui.nvim",
    url = "https://github.com/MunifTanjim/nui.nvim"
  },
  ["nvim-cmp"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/nvim-cmp",
    url = "https://github.com/hrsh7th/nvim-cmp"
  },
  ["nvim-treesitter"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["papis.nvim"] = {
    config = { "\27LJ\2\n�\1\0\2\v\0\a\0\0196\2\0\0009\2\1\0029\2\2\0026\4\3\0009\4\4\4'\6\5\0006\a\0\0009\a\1\a9\a\6\a\18\t\1\0B\a\2\0026\b\0\0009\b\1\b9\b\6\b\18\n\0\0B\b\2\0A\4\2\0A\2\0\1K\0\1\0\16shellescape,papis update --set notes %s papis_id:%s\vformat\vstring\vsystem\afn\bvim�\4\0\1\v\0\23\0;4\1\4\0005\2\0\0>\2\1\0015\2\1\0>\2\2\0015\2\2\0>\2\3\0016\2\3\0'\4\4\0B\2\2\2\18\4\2\0009\2\5\2\18\5\0\0\18\6\1\0B\2\4\0026\3\6\0\18\5\2\0B\3\2\4X\6\2�:\b\1\a<\b\6\2E\6\3\3R\6�\1275\3\a\0'\4\b\0006\5\t\0009\5\n\5\18\a\2\0B\5\2\2&\4\5\4>\4\2\3'\4\v\0006\5\f\0009\5\r\5'\a\14\0B\5\2\2&\4\5\4>\4\t\3'\4\15\0006\5\3\0'\a\16\0B\5\2\0029\5\17\5&\4\5\4>\4\n\0036\4\18\0009\4\19\0049\4\20\4)\6\0\0)\a\0\0\21\b\3\0+\t\1\0\18\n\3\0B\4\6\0016\4\18\0009\4\21\4'\6\22\0B\4\2\1K\0\1\0\rnormal G\bcmd\23nvim_buf_set_lines\bapi\bvim\fversion\17neorg.config\14version: \r%Y-%m-%d\tdate\aos\14created: \vconcat\ntable\ftitle: \1\r\0\0\19@document.meta\0\18description: \18categories: [\f  notes\15  academia\15  readings\6]\0\0\t@end\5\vipairs\27format_display_strings\16papis.utils\frequire\1\4\0\0\ntitle\a%s\5\1\4\0\0\tyear\n(%s) \5\1\4\0\0\vauthor\b%s \5�\17\1\0\6\0F\0_6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\0025\3\t\0005\4\b\0=\4\n\0035\4\v\0=\4\f\3=\3\r\0025\3\15\0005\4\14\0=\4\16\0035\4\17\0=\4\18\0035\4\19\0=\4\20\3=\3\21\0026\3\22\0009\3\23\0039\3\24\3'\5\25\0B\3\2\2'\4\26\0&\3\4\3=\3\27\0023\3\28\0=\3\29\0025\3\30\0=\3\31\0025\3 \0005\4!\0=\4\"\0034\4\f\0005\5#\0>\5\1\0045\5$\0>\5\2\0045\5%\0>\5\3\0045\5&\0>\5\4\0045\5'\0>\5\5\0045\5(\0>\5\6\0045\5)\0>\5\a\0045\5*\0>\5\b\0045\5+\0>\5\t\0045\5,\0>\5\n\0045\5-\0>\5\v\4=\4.\0034\4\4\0005\5/\0>\5\1\0045\0050\0>\5\2\0045\0051\0>\5\3\4=\0042\3=\0033\0025\0037\0004\4\4\0005\0054\0>\5\1\0045\0055\0>\5\2\0045\0056\0>\5\3\4=\0048\3=\0039\0025\3;\0003\4:\0=\4<\3=\3=\0025\3?\0005\4>\0=\4@\0035\4A\0=\4B\3=\3C\0025\3D\0=\3E\2B\0\2\1K\0\1\0\blog\1\0\2\nlevel\boff\18notify_format\a%s\18papis-storage\18required_keys\1\3\0\0\rpapis_id\bref\25key_name_conversions\1\0\0\1\0\1\15time_added\15time-added\14formatter\20format_notes_fn\1\0\0\0\19cursor-actions\17popup_format\1\0\0\1\4\0\0\ntitle\a%s\20PapisPopupTitle\1\4\0\0\tyear\a%s\19PapisPopupYear\1\4\0\0\vauthor\a%s\21PapisPopupAuthor\vsearch\19results_format\1\4\0\0\ntitle\a%s\22PapisResultsTitle\1\4\0\0\tyear\n(%s) \21PapisResultsYear\1\4\0\0\vauthor\b%s \23PapisResultsAuthor\19preview_format\1\a\0\0\rabstract\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\fjournal\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\nnotes\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\nfiles\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\ttags\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\ttype\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\bref\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\2\0\0\15empty_line\1\4\0\0\ntitle\a%s\22PapisPreviewTitle\1\4\0\0\tyear\a%s\21PapisPreviewYear\1\4\0\0\vauthor\a%s\23PapisPreviewAuthor\16search_keys\1\6\0\0\vauthor\veditor\tyear\ntitle\ttags\1\0\1\twrap\2\19init_filenames\1\4\0\0\16%info_name%\t*.md\v*.norg\23create_new_note_fn\0\fdb_path!/papis_db/papis-nvim.sqlite3\tdata\fstdpath\afn\bvim\20data_tbl_schema\bref\1\2\2\0\ttext\vunique\2\rrequired\2\rpapis_id\1\2\2\0\ttext\vunique\2\rrequired\2\aid\1\0\f\ntitle\ttext\tyear\ttext\veditor\ttext\vauthor\ttext\fjournal\ttext\ttype\ttext\nfiles\rluatable\ttags\rluatable\16author_list\rluatable\nnotes\rluatable\15time_added\ttext\rabstract\ttext\1\2\1\0\finteger\apk\2\17cite_formats\borg\1\3\0\0\15[cite:@%s]\16%[cite:@%s]\btex\1\0\4\nplain\a%s\brmd\b@%s\rmarkdown\b@%s\tnorg\v{= %s}\1\3\0\0\14\\cite{%s}\22\\cite[tp]?%*?{%s}\19enable_modules\1\0\a\vsearch\2\ndebug\1\tbase\2\vcolors\2\14formatter\2\19cursor-actions\2\15completion\2\17papis_python\1\0\5\19enable_keymaps\1\vyq_bin\ayq\22enable_fs_watcher\2\20enable_commands\2\26cite_formats_fallback\nplain\1\0\3\bdir /home/alex/Documents/papers\15notes_name\15notes.norg\14info_name\14info.yaml\nsetup\npapis\frequire\0" },
    load_after = {},
    loaded = true,
    needs_bufread = false,
    path = "/home/alex/.local/share/nvim/site/pack/packer/opt/papis.nvim",
    url = "https://github.com/jghauser/papis.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  ["sqlite.lua"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/sqlite.lua",
    url = "https://github.com/kkharji/sqlite.lua"
  },
  ["telescope.nvim"] = {
    loaded = true,
    path = "/home/alex/.local/share/nvim/site/pack/packer/start/telescope.nvim",
    url = "https://github.com/nvim-telescope/telescope.nvim"
  }
}

time([[Defining packer_plugins]], false)
-- Load plugins in order defined by `after`
time([[Sequenced loading]], true)
vim.cmd [[ packadd nvim-cmp ]]
vim.cmd [[ packadd telescope.nvim ]]
vim.cmd [[ packadd papis.nvim ]]

-- Config for: papis.nvim
try_loadstring("\27LJ\2\n�\1\0\2\v\0\a\0\0196\2\0\0009\2\1\0029\2\2\0026\4\3\0009\4\4\4'\6\5\0006\a\0\0009\a\1\a9\a\6\a\18\t\1\0B\a\2\0026\b\0\0009\b\1\b9\b\6\b\18\n\0\0B\b\2\0A\4\2\0A\2\0\1K\0\1\0\16shellescape,papis update --set notes %s papis_id:%s\vformat\vstring\vsystem\afn\bvim�\4\0\1\v\0\23\0;4\1\4\0005\2\0\0>\2\1\0015\2\1\0>\2\2\0015\2\2\0>\2\3\0016\2\3\0'\4\4\0B\2\2\2\18\4\2\0009\2\5\2\18\5\0\0\18\6\1\0B\2\4\0026\3\6\0\18\5\2\0B\3\2\4X\6\2�:\b\1\a<\b\6\2E\6\3\3R\6�\1275\3\a\0'\4\b\0006\5\t\0009\5\n\5\18\a\2\0B\5\2\2&\4\5\4>\4\2\3'\4\v\0006\5\f\0009\5\r\5'\a\14\0B\5\2\2&\4\5\4>\4\t\3'\4\15\0006\5\3\0'\a\16\0B\5\2\0029\5\17\5&\4\5\4>\4\n\0036\4\18\0009\4\19\0049\4\20\4)\6\0\0)\a\0\0\21\b\3\0+\t\1\0\18\n\3\0B\4\6\0016\4\18\0009\4\21\4'\6\22\0B\4\2\1K\0\1\0\rnormal G\bcmd\23nvim_buf_set_lines\bapi\bvim\fversion\17neorg.config\14version: \r%Y-%m-%d\tdate\aos\14created: \vconcat\ntable\ftitle: \1\r\0\0\19@document.meta\0\18description: \18categories: [\f  notes\15  academia\15  readings\6]\0\0\t@end\5\vipairs\27format_display_strings\16papis.utils\frequire\1\4\0\0\ntitle\a%s\5\1\4\0\0\tyear\n(%s) \5\1\4\0\0\vauthor\b%s \5�\17\1\0\6\0F\0_6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\0025\3\t\0005\4\b\0=\4\n\0035\4\v\0=\4\f\3=\3\r\0025\3\15\0005\4\14\0=\4\16\0035\4\17\0=\4\18\0035\4\19\0=\4\20\3=\3\21\0026\3\22\0009\3\23\0039\3\24\3'\5\25\0B\3\2\2'\4\26\0&\3\4\3=\3\27\0023\3\28\0=\3\29\0025\3\30\0=\3\31\0025\3 \0005\4!\0=\4\"\0034\4\f\0005\5#\0>\5\1\0045\5$\0>\5\2\0045\5%\0>\5\3\0045\5&\0>\5\4\0045\5'\0>\5\5\0045\5(\0>\5\6\0045\5)\0>\5\a\0045\5*\0>\5\b\0045\5+\0>\5\t\0045\5,\0>\5\n\0045\5-\0>\5\v\4=\4.\0034\4\4\0005\5/\0>\5\1\0045\0050\0>\5\2\0045\0051\0>\5\3\4=\0042\3=\0033\0025\0037\0004\4\4\0005\0054\0>\5\1\0045\0055\0>\5\2\0045\0056\0>\5\3\4=\0048\3=\0039\0025\3;\0003\4:\0=\4<\3=\3=\0025\3?\0005\4>\0=\4@\0035\4A\0=\4B\3=\3C\0025\3D\0=\3E\2B\0\2\1K\0\1\0\blog\1\0\2\nlevel\boff\18notify_format\a%s\18papis-storage\18required_keys\1\3\0\0\rpapis_id\bref\25key_name_conversions\1\0\0\1\0\1\15time_added\15time-added\14formatter\20format_notes_fn\1\0\0\0\19cursor-actions\17popup_format\1\0\0\1\4\0\0\ntitle\a%s\20PapisPopupTitle\1\4\0\0\tyear\a%s\19PapisPopupYear\1\4\0\0\vauthor\a%s\21PapisPopupAuthor\vsearch\19results_format\1\4\0\0\ntitle\a%s\22PapisResultsTitle\1\4\0\0\tyear\n(%s) \21PapisResultsYear\1\4\0\0\vauthor\b%s \23PapisResultsAuthor\19preview_format\1\a\0\0\rabstract\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\fjournal\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\nnotes\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\nfiles\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\ttags\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\ttype\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\a\0\0\bref\a%s\22PapisPreviewValue\rshow_key\n%s = \20PapisPreviewKey\1\2\0\0\15empty_line\1\4\0\0\ntitle\a%s\22PapisPreviewTitle\1\4\0\0\tyear\a%s\21PapisPreviewYear\1\4\0\0\vauthor\a%s\23PapisPreviewAuthor\16search_keys\1\6\0\0\vauthor\veditor\tyear\ntitle\ttags\1\0\1\twrap\2\19init_filenames\1\4\0\0\16%info_name%\t*.md\v*.norg\23create_new_note_fn\0\fdb_path!/papis_db/papis-nvim.sqlite3\tdata\fstdpath\afn\bvim\20data_tbl_schema\bref\1\2\2\0\ttext\vunique\2\rrequired\2\rpapis_id\1\2\2\0\ttext\vunique\2\rrequired\2\aid\1\0\f\ntitle\ttext\tyear\ttext\veditor\ttext\vauthor\ttext\fjournal\ttext\ttype\ttext\nfiles\rluatable\ttags\rluatable\16author_list\rluatable\nnotes\rluatable\15time_added\ttext\rabstract\ttext\1\2\1\0\finteger\apk\2\17cite_formats\borg\1\3\0\0\15[cite:@%s]\16%[cite:@%s]\btex\1\0\4\nplain\a%s\brmd\b@%s\rmarkdown\b@%s\tnorg\v{= %s}\1\3\0\0\14\\cite{%s}\22\\cite[tp]?%*?{%s}\19enable_modules\1\0\a\vsearch\2\ndebug\1\tbase\2\vcolors\2\14formatter\2\19cursor-actions\2\15completion\2\17papis_python\1\0\5\19enable_keymaps\1\vyq_bin\ayq\22enable_fs_watcher\2\20enable_commands\2\26cite_formats_fallback\nplain\1\0\3\bdir /home/alex/Documents/papers\15notes_name\15notes.norg\14info_name\14info.yaml\nsetup\npapis\frequire\0", "config", "papis.nvim")

time([[Sequenced loading]], false)

_G._packer.inside_compile = false
if _G._packer.needs_bufread == true then
  vim.cmd("doautocmd BufRead")
end
_G._packer.needs_bufread = false

if should_profile then save_profiles() end

end)

if not no_errors then
  error_msg = error_msg:gsub('"', '\\"')
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
